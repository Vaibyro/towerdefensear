# Tower Defense - AR
------------------------------

## Contributor
SÃ©bastien POLIN-MARCILLY
Alexandre MARIE
Thomas CAPODANO
Ludovic HAMEL

# Demonstration
You can find the trailer of the game here : [Trailer](https://youtu.be/y0IvjMEE_jg)
This video is located in folder `Video`.  

## How to play ?
### Marker
Print the marker to play.
File : `Marqueurs.pdf`

### App
Install the APK file on your **Android** smartphone.  
File : `APK/TowerDefense.apk`

### Usage
- Select your level
- Set position for you defense entities with markers
- Launch game
- During the game :
  - target the enemy with the camera
  - Touch the enemy to launch a missile

## Personal report
You can find all personal reports in the folder `Rapports`.
