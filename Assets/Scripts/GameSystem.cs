﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework.Constraints;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using UnityEngine.UI;


public enum GameState {
    PLAYING,
    WIN,
    LOSE
}

public class GameSystem : MonoBehaviour
{
    public GameObject LoseMessageLayout;
    public GameObject WinMessageLayout;
    public GameObject WarningPanel;
    public Spawner SpawnerObject;
    public int EntitiesRemaining { get; private set; }
    public TextMeshProUGUI EntitiesRemainingText;
    public Slider EntitiesRemainingProgressBar;
    public string NextLevelScene;
    public GameState State { get; set; } = GameState.PLAYING;
    public bool BuildingState = true;
    public GameObject SetupUIContainer;
    public int TurretsCount = 2;
    public TextMeshProUGUI TurretsCountText;
    
    private void Awake() {
        TurretsCountText.text = $"{TurretsCount}";
    }


  

    public void DisplayWarningPanel(bool state) {
        WarningPanel.SetActive(state);
    }
    
    // Start is called before the first frame update
    void Start() {
        
    }

    public void Launch() {
        SetupUIContainer.SetActive(false);
        BuildingState = false;
        
        // Load turrets (skin change)
        foreach (var turret in GameObject.FindGameObjectsWithTag("Turet")) {
            var genericTurret = turret.GetComponent<GenericTurretBehavior>();
            genericTurret.Loaded = true;
        }

        SpawnerObject.Launch();

        foreach (var markerObj in GameObject.FindGameObjectsWithTag("MobileMarker")) {
            markerObj.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update() {
        
        
        if (State == GameState.PLAYING) {
            EntitiesRemaining = SpawnerObject.EntitiesCount - EntityBehaviour.DeathCount;
            EntitiesRemainingText.text = $"{EntitiesRemaining}";
            EntitiesRemainingProgressBar.value = (float)EntityBehaviour.DeathCount / SpawnerObject.EntitiesCount;

            if (EntitiesRemaining == 0) {
                Win();
            }
        }
        
      
    }
    

    
    public void Lose()
    {
        State = GameState.LOSE;
        LoseMessageLayout.SetActive(true);
    }

    public void Win() {
        State = GameState.WIN;
        WinMessageLayout.SetActive(true);
    }

    public void Replay() {
        EntityBehaviour.DeathCount = 0; // Reset antities
        WinMessageLayout.SetActive(false);
        LoseMessageLayout.SetActive(false);
        
        var scene = SceneManager.GetActiveScene(); 
        SceneManager.LoadScene(scene.name);
    }

    public void NextLevel() {
        EntityBehaviour.DeathCount = 0; // Reset antities
        WinMessageLayout.SetActive(false);
        LoseMessageLayout.SetActive(false);
        SceneManager.LoadScene(NextLevelScene);
    }

    public void Quit() {
        Application.Quit();
    }
}
