﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TitleScreenMenu : MonoBehaviour {
    public GameObject PanelLevelsMenu;
    public string SceneName;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void DisplayLevelsMenu() {
        PanelLevelsMenu.SetActive(true);
    }
    
    public void HideLevelsMenu() {
        PanelLevelsMenu.SetActive(false);
    }

    public void Quit() {
        Application.Quit();
    }
    
    public void GoToLevel(int level)
    {
        SceneManager.LoadScene($"Level{level}");
    }
}
