﻿using System.Collections;
using System.Collections.Generic;

using UnityEditor;
using UnityEngine;

public class Path : MonoBehaviour
{

    public List<Vector3> PassagePoints;


    
    // Start is called before the first frame update
    void Start() {
       
    }

    // Update is called once per frame
    void Update() {
      
    }

    void OnDrawGizmos()
    {
        if (PassagePoints.Count == 0)
        {
            return;
        }
        
        Gizmos.color = Color.cyan;
        
        Gizmos.DrawLine(GameObject.FindWithTag("Spawn").transform.position, PassagePoints[0]);
        for (var i = 0; i < PassagePoints.Count; i++)
        {
            Gizmos.DrawSphere(PassagePoints[i], 0.1f);
            //Handles.Label(PassagePoints[i] + new Vector3(0, 0.5f, 0), $"{i+1}");

            Gizmos.DrawLine(PassagePoints[i],
                i == PassagePoints.Count - 1
                    ? GameObject.FindWithTag("Base").transform.position
                    : PassagePoints[i + 1]);
        }

        Gizmos.color = Color.white;
    }
}
