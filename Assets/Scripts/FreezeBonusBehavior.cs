﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using TMPro;
using UnityEngine;
using Vuforia;
using Debug = UnityEngine.Debug;

public class FreezeBonusBehavior : MonoBehaviour
{

    public float SpeedMultiplier = 0.5f;
    public int Time = 3;
    public GameObject BuffText;
    private GameSystem _game;
    
    
    private bool Consumed = false;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<VirtualButtonBehaviour>().RegisterOnButtonPressed(OnButtonPressed);
        GetComponent<VirtualButtonBehaviour>().RegisterOnButtonReleased(OnButtonReleased);
        _game = GameObject.FindWithTag("GameSystem").GetComponent<GameSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    
    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        Debug.LogWarning("Button pressed");

        if (!Consumed && !_game.BuildingState)
        {
            StartCoroutine(nameof(Timer), Time);
          
            BuffText.SetActive(true);
            
            foreach (var entity in GameObject.FindGameObjectsWithTag("Entity"))
            {
                var e = entity.GetComponent<EntityBehaviour>();
                e.SetSpeedMultiplier(SpeedMultiplier);
            }
            
            Consumed = true;
        }
    }

    public void FinishBuff()
    {
        foreach (var entity in GameObject.FindGameObjectsWithTag("Entity"))
        {
            var e = entity.GetComponent<EntityBehaviour>();
            e.ResetSpeedMultiplier();
        }
        
        BuffText.SetActive(false);
    }

    public void SetEntitiesSpeed(float speed)
    {
        foreach (var entity in GameObject.FindGameObjectsWithTag("Entity"))
        {
            var e = entity.GetComponent<EntityBehaviour>();
            e.SetSpeedMultiplier(speed);
        }
    }
    
    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        Debug.LogWarning("Button released");
    }

    IEnumerator Timer(int seconds)
    {
        var counter = seconds;
        while (counter > 0)
        {
            Debug.Log(counter);
            yield return new WaitForSeconds(1);
            counter--;
        }
        
        FinishBuff();
    }
}