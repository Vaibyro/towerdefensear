﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGroundClampTarget : MonoBehaviour
{
    public Transform Target;
    public Transform Turet;
    public Terrain Terrain;

    private GameSystem _gameSystem;
    
    // Start is called before the first frame update
    void Awake()
    {
        Turet.gameObject.SetActive(false);
    }

    private void Start()
    {
        _gameSystem = GameObject.FindWithTag("GameSystem").GetComponent<GameSystem>();
    }

    public void OnTargetFound()
    {
        Turet.gameObject.SetActive(true);
    }
    
    public void OnTargetLost()
    {
        if (_gameSystem != null && _gameSystem.BuildingState)
        {
            Turet.gameObject.SetActive(false);
        }
    }
    
    // Update is called once per frame
    void Update()
    {
        if (_gameSystem.BuildingState)
        {
            float height = Terrain.SampleHeight(new Vector3((int) Target.position.x, 0, (int) Target.position.z));
            Turet.position = new Vector3(Target.position.x, height, Target.position.z);

            var eulerRotation = Target.rotation.eulerAngles;
            Turet.rotation = Quaternion.Euler(0f, eulerRotation.y, 0f);
        }
    }
}
