﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class AutoSound : MonoBehaviour {
    private AudioSource _audioSource;
    
    
    // Start is called before the first frame update
    void Start() {
        _audioSource = GetComponent<AudioSource>();
        _audioSource.loop = false;
        _audioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if (_audioSource.isPlaying) {
            return;
        }
        
        GameObject.Destroy(gameObject);
    }
}
