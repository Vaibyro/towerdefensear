﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BaseBehavior : MonoBehaviour
{
    public int MaxHealth = 50;
    public Slider HpBar;
    public TextMeshProUGUI Text;
    
    private int _currentHealth;
    private SphereCollider _triggerEntitiesZone;

    public GameObject ExplosionPrefab;
    public GameObject ExplosionSoundPrefab;

    public void Damage(int amount)
    {
        if (_currentHealth - amount <= 0)
        {
            _currentHealth = 0;

            // Launch lose
            GameObject.FindWithTag("GameSystem").GetComponent<GameSystem>().Lose();
        }
        else
        {
            _currentHealth -= amount;
        }
        
        SetUi();
    }

    // Start is called before the first frame update
    void Start()
    {
        _triggerEntitiesZone = GetComponent<SphereCollider>();
        _currentHealth = MaxHealth;
        SetUi();
    }

    public void SetUi()
    {
        Text.text = $"{_currentHealth} / {MaxHealth}";
        HpBar.value = (float)_currentHealth / MaxHealth;
    }
    
    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Entity"))
        {
            Debug.Log("aie");
            
            var explosion = Instantiate(ExplosionPrefab, other.transform.position, Quaternion.identity);
            explosion.transform.localScale = new Vector3(1f, 1f, 1f);

            var entity = other.GetComponent<EntityBehaviour>();
            Damage(entity.TowerDamage);
            
            Instantiate(ExplosionSoundPrefab, transform.position, Quaternion.identity); // Sound
            
            entity.Kill();
        }    
    }
}
