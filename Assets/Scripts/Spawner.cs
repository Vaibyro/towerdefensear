﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Spawner : MonoBehaviour
{
    

    public float TimeBetweenTwoEntities = 2f;
    public GameObject SpawnSoundPrefab;
    public List<EntityBehaviour> SpawnPrefabs = new List<EntityBehaviour>();
    
    public int EntitiesCount {
        get => spawPattern.Count;
    }
    
    
    public List<int> spawPattern = new List<int>();
    
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start launching entities");
      
        
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Launch() {
        StartCoroutine(nameof(SpawnLoop));
    }

    private IEnumerator SpawnLoop()
    {
        for (var i = 0; i < EntitiesCount; i++)
        {
            Debug.Log($"Launch entity no. {i}");
            Instantiate(SpawnPrefabs[spawPattern[i]], gameObject.transform);
            
            Instantiate(SpawnSoundPrefab, transform.position, Quaternion.identity); // Sound
            
            yield return new WaitForSeconds(TimeBetweenTwoEntities);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawSphere(transform.position, 0.3f);
    }
}
