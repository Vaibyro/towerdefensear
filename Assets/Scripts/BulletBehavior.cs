﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehavior : MonoBehaviour
{
    public float Speed = 1f;
    public int Damage = 10;
    public EntityBehaviour Target;
    public TuretBehavior SourceTurret;
    public float Delta;
    public GameObject ExplosionPrefab;
    public GameObject ExplosionSoundPrefab;
    
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // Move forward
        transform.position += transform.forward * Speed * Time.deltaTime;

        if (Target == null)
        {
            if (Vector3.Distance(transform.position, SourceTurret.transform.position) > 50f)
            {
                Destroy(gameObject);
            }
            return;
        }
        
        if (Vector3.Distance(transform.position, Target.transform.position) < Delta)
        {
            Debug.Log("touché");
            
            var explosion = Instantiate(ExplosionPrefab, transform.position, Quaternion.identity);
            explosion.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            
            Instantiate(ExplosionSoundPrefab, transform.position, Quaternion.identity); // Sound
            
            if (!Target.Damage(Damage)) {
                SourceTurret.IndicateMobDeath(Target);
            }
            
            Destroy(gameObject);
            return;
        }

        if (Vector3.Distance(transform.position, SourceTurret.transform.position) >
            1.5 * Vector3.Distance(Target.transform.position, SourceTurret.transform.position))
        {
            Destroy(gameObject);
            return;
        }
    }
}
