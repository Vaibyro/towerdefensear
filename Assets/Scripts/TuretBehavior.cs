﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TuretBehavior : GenericTurretBehavior
{
    public float Radius = 2f;
    public Vector3 Center = Vector3.zero;
    public GameObject Bullet;
    public float Cooldown = 0.5f;
    public GameObject canon;
    public GameObject FieldIndicator;
    
    private Color currentSkin;
    private SphereCollider _sphereCollider;
    private readonly List<EntityBehaviour> _entitiesInZone = new List<EntityBehaviour>();

    public GameObject ForceField;
    
    // Start is called before the first frame update
    void Awake()
    {
        gameObject.AddComponent<SphereCollider>();
        _sphereCollider = GetComponent<SphereCollider>();
        _sphereCollider.radius = Radius;
        _sphereCollider.center = Center;
        _sphereCollider.isTrigger = true;
    }
    
    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Entity"))
        {
            _entitiesInZone.Add(other.GetComponent<EntityBehaviour>());
            Debug.Log($"Entity entered the DEATH ZONE, {_entitiesInZone.Count} entities inside the zone");
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Entity"))
        {
            _entitiesInZone.Remove(other.GetComponent<EntityBehaviour>());
            Debug.Log($"Entity is now safe, {_entitiesInZone.Count} entities inside the zone");
        }
    }

    void Start()
    {
        StartCoroutine("FireLoop");
    }
    
    // Update is called once per frame
    void Update() {
        FieldIndicator.transform.localScale = new Vector3(Radius, Radius, Radius) * 2.0f;
        
        if (_entitiesInZone.Count > 0)
        {
            var mob = _entitiesInZone[0];

            if (mob != null)
            {
                var entity = mob.GetComponent<EntityBehaviour>();
                if (entity.IsDead)
                {
                    IndicateMobDeath(entity);
                }
                else
                {
                    var direction = mob.transform.position - transform.position;
                    transform.rotation = Quaternion.LookRotation(direction);
                }
            }
           
        }
        
        if (Loaded && currentSkin != Color.cyan)
        {
            var meshRenderer = ForceField.GetComponent<Renderer>();
            meshRenderer.material.SetColor("_Color", new Color(0, 1, 1, 0.8f));
            currentSkin = Color.cyan;
            
        }
    }

    IEnumerator FireLoop()
    {
        while (true)
        {
            // Remove the first null
            while (_entitiesInZone.Count > 0 && _entitiesInZone[0] == null)
            {
                _entitiesInZone.RemoveAt(0);
            }
            
            if (_entitiesInZone.Count > 0)
            {
                var mob = _entitiesInZone[0];
                var direction = mob.transform.position - transform.position + mob.transform.forward * 0.5f;
                var bulletInstance = Instantiate(Bullet, canon.transform.position, Quaternion.LookRotation(direction));
                var bulletBehavior = bulletInstance.GetComponent<BulletBehavior>();
                bulletBehavior.Target = mob;
                bulletBehavior.SourceTurret = this;
            }
            
            yield return new WaitForSeconds(Cooldown);
        }
    }

    public void IndicateMobDeath(EntityBehaviour mob)
    {
        if (_entitiesInZone.Contains(mob))
        {
            _entitiesInZone.Remove(mob);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(Center + transform.position, Radius);
    }
}
