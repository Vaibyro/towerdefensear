﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class EntityBehaviour : MonoBehaviour
{
    private GameObject _baseGameObject;
    private NavMeshAgent _navMeshAgent;
    private Path _path;
    private int _currentPoint = 0;
    private bool _isDead = false;
    private float _baseSpeed = 0;
    
    public bool IsDead => _isDead;
    public int Health = 10;
    public static int DeathCount = 0;
    public int TowerDamage = 10;

    public GameObject DeathSfxPrefab;
    
    // Start is called before the first frame update
    void Start()
    {
        _baseGameObject = GameObject.FindWithTag("Base");
        _path = GameObject.FindWithTag("Path").GetComponent<Path>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _navMeshAgent.destination = _path.PassagePoints[_currentPoint++];
        _baseSpeed = _navMeshAgent.speed;
    }

    public bool Damage(int hp)
    {
        if (Health - hp <= 0)
        {
            Kill();
            return false;
        }

        Health -= hp;
        
        return true;
    }

    
    
    public void SetSpeedMultiplier(float multiplier)
    {
        _navMeshAgent.speed = _baseSpeed * multiplier;
    }

    public void ResetSpeedMultiplier()
    {
        _navMeshAgent.speed = _baseSpeed;
    }
    
    public void Kill()
    {
        if (!_isDead) {
            DeathCount++;
            _isDead = true;
            
            Instantiate(DeathSfxPrefab, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
    
    // Update is called once per frame
    void Update()
    {

        if (Reached())
        {
            if (_currentPoint < _path.PassagePoints.Count)
            {
                _navMeshAgent.destination = _path.PassagePoints[_currentPoint];
                _currentPoint++;
            }
            else if(_currentPoint == _path.PassagePoints.Count)
            {
                _navMeshAgent.destination = _baseGameObject.transform.position;
            }
        }
    }

    
    
    private bool Reached()
    {
        if (!_navMeshAgent.pathPending && !_isDead)
        {
            if (_navMeshAgent.remainingDistance <= _navMeshAgent.stoppingDistance)
            {
                if (!_navMeshAgent.hasPath || _navMeshAgent.velocity.sqrMagnitude == 0f)
                {
                    return true;
                }
            }
        }

        return false;
    }
}
