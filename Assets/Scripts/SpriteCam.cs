﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteCam : MonoBehaviour {
    public Camera Camera;

    public bool UseMainCam = true;
    // Start is called before the first frame update
    void Start()
    {
        if (UseMainCam) {
            Camera = Camera.main;
        }
    }

    // Update is called once per frame
    void Update() {
        var targetVector = transform.position - Camera.transform.position;
        transform.rotation = Quaternion.LookRotation(targetVector, Camera.transform.up);
    }
}