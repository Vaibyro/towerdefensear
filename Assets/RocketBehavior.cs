﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketBehavior : MonoBehaviour {
    public GameObject BigExplosionPrefab;
    public GameObject ExplosionSoundPrefab;
    public Transform CenterOfMass;
    private List<EntityBehaviour> _mobsInPerimeter = new List<EntityBehaviour>();


    
    public int Damage = 100;
    
    // Start is called before the first frame update
    void Start() {
        var rigidBody = GetComponent<Rigidbody>();
        rigidBody.centerOfMass = CenterOfMass.position;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision other) {
        //if (other.gameObject.layer == LayerMask.NameToLayer("Backdrop")) {

            Instantiate(BigExplosionPrefab, transform.position, Quaternion.identity);
            Instantiate(ExplosionSoundPrefab, transform.position, Quaternion.identity);
            
            // Damage mobs
            Debug.Log($"{_mobsInPerimeter.Count} mob(s) impacted by explosion");
            foreach (var mob in _mobsInPerimeter) {
                try {
                    mob.Damage(Damage);
                } catch (Exception e) {
                    // ignored
                }
            }
            
            // Destroy the rocket
            Destroy(gameObject);
        //}
    }
    
    private void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Entity")) {
            _mobsInPerimeter.Add(other.GetComponent<EntityBehaviour>());
        }
    }
}
