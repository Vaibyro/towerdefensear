﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class RocketScreenLaunch : MonoBehaviour {
    public GameObject RocketPrefab;
    private Camera _mainCamera;
    private GameSystem _gameSystem;

    public TextMeshProUGUI UIRockets;
    public int MaxLaunches = 2;

    private AudioSource _audioSource;
    
    // Start is called before the first frame update
    void Start()
    {
        _mainCamera = Camera.main;
        _gameSystem = GameObject.FindWithTag("GameSystem").GetComponent<GameSystem>();
        UIRockets.text = LaunchesText;
        _audioSource = GetComponent<AudioSource>();
    }
    
    private string LaunchesText => $"{MaxLaunches}";
    
    // Update is called once per frame
    void Update()
    {
        // Check if there is a touch
        if (Input.GetButtonDown("Fire1") && !_gameSystem.BuildingState && MaxLaunches > 0) {

           
            
            MaxLaunches--;
            UIRockets.text = LaunchesText;
            var mousePos = Input.mousePosition;
            {

                RaycastHit hit;
                var ray = _mainCamera.ScreenPointToRay(Input.mousePosition);
                if (Physics.Raycast(ray, out hit, 1000, LayerMask.GetMask("Backdrop"))) {
                    var rocketInstance = Instantiate(RocketPrefab, _mainCamera.transform.position, Quaternion.identity);
                    var direction = (hit.point - _mainCamera.transform.position).normalized;
                    var rigidbody = rocketInstance.GetComponent<Rigidbody>();
                    
                    rocketInstance.transform.rotation = Quaternion.LookRotation(_mainCamera.transform.forward);
                    
                    rigidbody.AddForce(direction * 30000f);
                    _audioSource.Play();
                }
            }
        }
    }
}
